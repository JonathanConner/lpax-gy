<?php

namespace Api;

use User\Document\Relationship\Connection;
use Zend\Authentication\AuthenticationService;
use Zend\ModuleManager\ModuleManager;
use Zend\EventManager\EventInterface as Event;
use User\Document\Event\NotificationEvent;
use Doctrine\ODM\MongoDB\Events;
use Zend\Router\RouteMatch;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\ResponseInterface;

class Module
{

    const VERSION = '3.0.3-dev';

    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }



}
