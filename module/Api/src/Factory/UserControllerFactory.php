<?php
namespace Api\Factory;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\Factory\FactoryInterface;
use Api\Controller\UserController;
class UserControllerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $dm = $container->get('doctrine.documentmanager.odm_default');
        return new UserController($dm);
    }
}