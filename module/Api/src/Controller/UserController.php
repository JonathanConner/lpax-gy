<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Api\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;

class UserController extends AbstractRestfulController
{


    /**
     * @var DocumentManager
     */
    protected $dm;

    public function __construct(DocumentManager $dm)
    {
        $this->dm = $dm;
    }


    /**
     * @return JsonModel
     */
    public function getList()
    {
        try {
            $users = $this->dm->getRepository('User\Document\User')->findBy(
                ['status' => 'active']
            );
        } catch (MongoDBException $mdbe) {
            return new JsonModel(['message' => $mdbe->getMessage()]);
        }

        $data = [];
        foreach ($users as $user) {
            $data[] = $user->toArr();
        }


        return new JsonModel([
            //send data as well
            $data,
            ['message' => 'API Request Successful!']
        ]);
    }

    /**
     * @param mixed $id
     * @return JsonModel
     */
    public function get($id)
    {
        /*        $book = $this->entityManager->getRepository(Book::class)->find($id);
                if (!$book) {
                    return new JsonModel(['message' => 'Id not found']);
                }*/
        return new JsonModel([
            //send data as well
            'message' => '!'
        ]);
    }

    /**
     * @param mixed $data
     * @return JsonModel
     */
    public function create($data)
    {

        return new JsonModel([
            //send data as well
            'message' => '!'
        ]);
    }

    /**
     * @param mixed $id
     * @param mixed $data
     * @return JsonModel
     */
    public function update($id, $data)
    {
        if (!$id) {
            return new JsonModel(['message' => 'Id not found']);
        }

        return new JsonModel([
            //send data as well
            'message' => '!'
        ]);
    }

    /**
     * @param mixed $id
     * @return JsonModel
     */
    public function delete($id)
    {
        if (!$id) {
            return new JsonModel(['message' => 'Id not found']);
        }

        return new JsonModel([
            'message' => '!'
        ]);
    }

}
