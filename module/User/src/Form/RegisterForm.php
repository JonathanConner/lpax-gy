<?php

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element\Email;
use Zend\Form\Element\Text;
use Zend\Form\Element\Password;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Submit;

class RegisterForm extends Form
{


    public function __construct()
    {

        parent::__construct();

        $this->setAttribute('method', 'post')
            ->setAttribute('action', '/user/register')
            ->setAttribute('id', 'user-register-form');

        $email = new Email();
        $email->setName('email')
              ->setLabel('Email Address');

        $userName = new Text();
        $userName->setName('userName')
                 ->setLabel('Username');

        $password = new Password();
        $password->setName('password')
                 ->setLabel('Password');

        $firstName = new Text();
        $firstName->setName('firstName')
                  ->setLabel('First Name');

        $lastName = new Text();
        $lastName->setName('lastName')
                 ->setLabel('Last Name');

        $csrf = new Csrf();
        $csrf->setName('prev');

        $submit = new Submit();
        $submit->setName('submit')
               ->setValue('Register');

        $this->add($email)
            ->add($userName)
            ->add($password)
            ->add($firstName)
            ->add($lastName)
            ->add($csrf)
            ->add($submit);

        foreach ($this->elements as $element) {
            if (! $element instanceof Submit)
                $element->setAttribute('class', 'form-control');

        }

    }

}