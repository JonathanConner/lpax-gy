<?php

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Button;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Textarea;
use Zend\Form\Element\Text;

/**
 *
 * @author Jon Conner
 *
 *
 */
class MessageForm extends Form
{

    public function __construct()
    {
        parent::__construct();


        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/user/dashboard/messages/create');
        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('id', 'user-message-form');


        $to = new Text();
        $to->setName('recipient')
           ->setLabel('Recipients')->setAttribute('id', 'recipients');

        $subject = new Text();
        $subject->setName('subject')
                ->setLabel('Subject');

        $body = new Textarea();
        $body->setName('body')
             ->setAttributes(array('placeholder'=>'Your message...', 'rows'=>4));

        $csrf = new Csrf();
        $csrf->setName('prev');

        $submit = new Submit();
        $submit->setName('submit')
               ->setValue('Send');

        $this->add($to)
             ->add($subject)
             ->add($body)
             ->add($csrf)
             ->add($submit);


        foreach ($this->elements as $element) {
            if (! $element instanceof Submit)
                $element->setAttribute('class', 'form-control');
        }
    }
}