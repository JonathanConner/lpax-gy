<?php

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element\Email;
use Zend\Form\Element\Password;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Submit;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\DocumentManager;
use Zend\Form\Element\Checkbox;

class LoginForm extends Form
{


    public function __construct()
    {

        parent::__construct();
        
        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/user/login');
        $this->setAttribute('class', 'form');
        $this->setAttribute('id', 'userLoginForm');
        $this->setAttribute('role', 'form');



        $email = new Email();
        $email->setName('email')
            ->setLabel('Email Address')
            ->setAttribute('required', 'true');
        
        $password = new Password();
        $password->setName('password')
            ->setLabel('Password')
            ->setAttribute('required', 'true');



        $csrf = new Csrf();
        $csrf->setName('prev');
        
        $checkbox = new Checkbox();
        $checkbox->setName('remember-me');
        $checkbox->setOptions([
            'use_hidden_element' => false  ,
            'required' => false,
        ]);
        $checkbox->setChecked("checked");


        $submit = new Submit();
        $submit->setName('submit')
            ->setValue('Sign In');

        $this->add($email)
            ->add($password)
            ->add($checkbox)
            ->add($csrf)
            ->add($submit);



    }

}