<?php

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element\Csrf;
use Zend\Form\Element\Button;
use Zend\Form\Element\Submit;
use Zend\Form\Element\Textarea;
use Zend\Form\Element\Text;

/**
 *
 * @author Jon Conner
 */
class PostForm extends Form
{


    public function __construct()
    {

        parent::__construct();

        $this->setAttribute('method', 'post');
        $this->setAttribute('action', '/user/dashboard/post/create');
        $this->setAttribute('id', 'user-post-form');

        $title = new Text();
        $title->setName('title')
            ->setLabel('Title')
            ->setAttribute('required', 'true');

        $body = new Textarea();
        $body->setName('body')->setAttributes(array(
            'placeholder' => 'Your post content...',
            'rows' => 8,
            'resizable' => 'false',
            'required' => 'true'
        ));

        $csrf = new Csrf();
        $csrf->setName('prev');

        $submit = new Submit();
        $submit->setName('submit')
            ->setValue('Create')
            ->setAttribute('class', 'btn btn-info');

        $this->add($title)
            ->add($body)
            ->add($csrf)
            ->add($submit);


        foreach ($this->elements as $element) {
            if (! $element instanceof Submit)
                $element->setAttribute('class', 'form-control');
        }

    }

}