<?php
namespace User\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use User\Document\User;
use User\Service\DocumentManagerAwareInterface;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;


class DashboardController extends AbstractActionController implements DocumentManagerAwareInterface
{

    /**
     * @type DocumentManager
     */
    protected $dm;


    public function setDocumentManager(DocumentManager $dm)
    {

        $this->dm = $dm;

    }


    /**
     * @return mixed
     */
    public function indexAction()
    {

        /**
         * @type $user User
         */
        $user = $this->identity();

//        $image = $this->dm->createQueryBuilder('User\Document\Social\Album')->find(['owner' => $user->getId()])->getQuery()->execute();

        $posts = null;

        foreach ($user->getPosts() as $userPost) {
            $posts[] = $userPost;
        }

        $view = new ViewModel([
            'posts' => $posts
        ]);


        $view->setTemplate('dashboard/index');

        return $this->dashboardBuilder($view, $this->layout());

    }


    public function addfriendAction()
    {


    }

}