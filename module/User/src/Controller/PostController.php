<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\PostForm;
use Doctrine\ODM\MongoDB\DocumentManager;
use User\Service\DocumentManagerAwareInterface;
use User\Document\Post;

/**
 * PostController
 *
 * @author
 *
 * @version
 *
 */
class PostController extends AbstractActionController implements DocumentManagerAwareInterface
{

    protected $dm;


    public function setDocumentManager(DocumentManager $dm)
    {

        $this->dm = $dm;

    }


    public function indexAction()
    {

        $user = $this->identity();
        $posts = $user->getPosts();

        $form = new PostForm();

        $view = new ViewModel([
            'posts' => $posts,
            'form' => $form
        ]);

        $view->setTemplate('dashboard/post');

        return $this->dashboardBuilder($view, $this->layout());

    }


    public function createAction()
    {

        $user = $this->identity();

        $form = new PostForm();
        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($request->getPost());

            if ($form->isValid()) {

                $data = $form->getData();

                    // Post does not already exist so continue creation
                    $post = new Post($user, $data['title'], $data['body']);

                    // Save documents
                    $this->dm->persist($post);
                    $this->dm->persist($user);
                    $this->dm->flush();

                    //Set flash success message and redirect
                    $this->flashMessenger()->addSuccessMessage("New post created successfully!");
                    return $this->redirect()->toRoute('user_dashboard/post');

            }
        }
    }

}