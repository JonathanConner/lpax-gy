<?php
namespace User\Controller;

use User\Service\DocumentManagerAwareInterface;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\Session\SessionManager;
use Zend\View\Model\ViewModel;
use User\Document\User;
use Zend\Authentication\AuthenticationService;
use User\Form\LoginForm;
use Doctrine\ODM\MongoDB\DocumentManager;

class LoginController extends AbstractActionController implements DocumentManagerAwareInterface
{

    protected $dm;


    public function setDocumentManager(DocumentManager $dm)
    {

        $this->dm = $dm;

    }


    public function indexAction()
    {

        $form = new LoginForm();
        $authService = $this->getEvent()->getApplication()->getServiceManager()->get('Zend\Authentication\AuthenticationService');

        $request = $this->getRequest();

        if ($request->isPost()) {

            $form->setData($request->getPost());

            if ($form->isValid()) {


                $data = $form->getData();
/** @var \Zend\Authentication\AuthenticationService $authService */
/** @var \Zend\Authentication\Adapter\AbstractAdapter $adapter */
                $adapter = $authService->getAdapter();
                $adapter->setIdentity($data['email']);
                $adapter->setCredential($data['password']);
                $authResult = $authService->authenticate();
                if ($authResult->isValid()) {
                    echo "Auth Valid";
                    $identity = $authResult->getIdentity();
                    $identity->setLastLogin();
                    $authService->getStorage()->write($identity);
                    $time = 1209600; // 14 days 1209600/3600 = 336 hours => 336/24 = 14 days
//-					if ($data['rememberme']) $authService->getStorage()->session->getManager()->rememberMe($time); // no way to get the session
                    if ($data['remember-me']) {
                        $sessionManager = new SessionManager();
                        $sessionManager->rememberMe($time);
                    }
                    return $this->redirect()->toRoute('user_dashboard');
                }
                else{
                    $this->flashMessenger()->addErrorMessage("Invalid login credentials provided. Try again, or sign up!");
                }
            }
        }

        $view = new ViewModel(array(
            'form' => $form
        ));
        return $view;

    }

}