<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ODM\MongoDB\DocumentManager;
use User\Document\User;

class AdminController extends AbstractActionController implements \User\Service\DocumentManagerAwareInterface
{

    protected $dm;

    public function setDocumentManager(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function indexAction()
    {
        $auth = $this->setServiceLocator()->get('Zend\Authentication\AuthenticationService');

        if (! $auth->hasIdentity()) {
            return $this->redirect()->toRoute('user_login');
        } else if($auth->getIdentity()){
            
        }
    }
}