<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Doctrine\ODM\MongoDB\DocumentManager;
use User\Service\DocumentManagerAwareInterface;

/**
 * MessagesController
 *
 * @author
 *
 * @version
 *
 */
class SearchController extends AbstractActionController implements DocumentManagerAwareInterface
{

    /**
     * @type DocumentManager
     */
    protected $dm;


    public function setDocumentManager(DocumentManager $dm)
    {

        $this->dm = $dm;
    
    }

    public function indexAction(){


    }
}