<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use User\Service\DocumentManagerAwareInterface;
use Doctrine\ODM\MongoDB\DocumentManager;

/**
 * LogoutController
 *
 * @author
 *
 * @version
 *
 */
class LogoutController extends AbstractActionController implements DocumentManagerAwareInterface
{

    protected $dm;

    public function setDocumentManager(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    /**
     * The default action - show the home page
     */
    public function indexAction()
    {

        $auth = $this->getEvent()->getApplication()->getServiceManager()->get('Zend\Authentication\AuthenticationService');
        if ($auth->hasIdentity()) {
            $auth->clearIdentity();
            $this->flashMessenger()->addSuccessMessage("You have logged out of the system!");
        }
        return $this->redirect()->toRoute('home');
    }
}