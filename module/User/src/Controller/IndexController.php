<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ODM\MongoDB\DocumentManager;
use User\Document\User;
use User\Service\DocumentManagerAwareInterface;
class IndexController extends AbstractActionController implements DocumentManagerAwareInterface
{

    protected $dm;

    public function setDocumentManager(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function indexAction()
    {

        if($this->request->getQuery())

        $auth = $this->getEvent()->getApplication()->getServiceManager()->get('Zend\Authentication\AuthenticationService');

        if (! $auth->hasIdentity()) {
            return $this->redirect()->toRoute('user_login');
        } else {
            return $this->redirect()->toRoute('user_dashboard');
        }
    }
}