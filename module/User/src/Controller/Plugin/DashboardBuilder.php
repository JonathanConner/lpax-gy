<?php
namespace User\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Model\ViewModel;

/**
 *
 * @author jonconner
 *
 */
class DashboardBuilder extends AbstractPlugin
{

    public function __invoke($dataView, $layout, $other=null)
    {

        $dashboard = new ViewModel();
        $dashboard->setTemplate('dashboard/view');

        $sidebar = new ViewModel();
        $sidebar->setTemplate('dashboard/sidebar');
        $sidebar->setVariable("user",$this->getController()->getPluginManager()->get("identity"));
        if(isset($other)){
            $aside = new ViewModel();
            $aside->setTemplate('dashboard/aside');
            $dashboard->addChild($aside, 'aside');
        }

        $dashboard->addChild($sidebar, 'sidebar');
        $dashboard->addChild($dataView, 'data');

        $layout->addChild($dashboard, 'content');

        return $dashboard;
    }

}