<?php
namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ODM\MongoDB\DocumentManager;
use User\Form\MessageForm;
use User\Document\Message\Message;
use Zend\View\Model\JsonModel;
use User\Service\DocumentManagerAwareInterface;

/**
 * MessagesController
 *
 * @author
 *
 * @version
 *
 */
class MessagesController extends AbstractActionController implements DocumentManagerAwareInterface
{

    protected $dm;


    public function setDocumentManager(DocumentManager $dm)
    {

        $this->dm = $dm;

    }


    public function indexAction()
    {

        $user = $this->identity();

        $messages = $user->getMessages();

        $form = new MessageForm();

        $view = new ViewModel(array(
            'messages' => $messages,
            'form' => $form
        ));

        $view->setTemplate('dashboard/messages');

        return $this->dashboardBuilder($view, $this->layout(), true);

    }


    public function createAction()
    {

        $user = $this->identity();
        $request = $this->getRequest();

        $form = new MessageForm();

        if ($request->isPost()) {

            $data = $request->getPost();
            $form->setData($data);

            if ($form->isValid()) {

                $data = $form->getData();

                $message = new Message($user, $data['subject'], $data['body']);

                foreach ($data['recipients'] as $recipient) {

                    $receiver = $this->dm->getRepository('User\\Document\\User')->findOneBy(array(
                        'id' => $recipient['id']
                    ));
                    $message->setRecipients($recipient);

                }

                $this->dm->persist($message);
                $this->dm->persist($user);

                $this->dm->flush();

                $this->flashMessenger()->addSuccessMessage("Message sent successfully!");
                return $this->redirect()->toRoute('user_dashboard/messages');

            } else {

                $this->flashMessenger()->addErrorMessage("There was a problem creating your message! Review the form and try again!");

                return $this->redirect()->toRoute('user_dashboard/messages');

            }
        }

    }


    public function markReadAction()
    {

        $request = $this->getRequest();
        if ($request->isGet()) {
            $data = $request->getQuery();

            $message= $this->dm->getRepository('User\\Document\\Message\\Message')
                                  ->findOneBy(array("id"=>$data['mid']));


            $message['status']=$data['status'];

            $this->dm->persist($message);
            $this->dm->flush();

            return new JsonModel(array('success'=>true));

        }

    }


    public function searchRecipientsAction()
    {

        $request = $this->getRequest();

        if ($request->isGet()) {
            $data = $request->getQuery();
            $username = $data['recipient'];
            $qb = $this->dm->createQueryBuilder('User\Document\User')
                ->field('userName')
                ->equals(new \MongoRegex("/{$username}/"));

            $query = $qb->getQuery();
            $users = $query->execute();

            $return_array = array();
            foreach ($users as $user1) {
                $return_array[] = $user1->getUsername();
            }

            return new \Zend\View\Model\JsonModel($return_array);
        }

    }

}