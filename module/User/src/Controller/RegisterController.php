<?php

namespace User\Controller;

use User\Service\UserManager;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Doctrine\ODM\MongoDB\DocumentManager;
use User\Document\User;
use Zend\Authentication\AuthenticationService;
use User\Form\RegisterForm;
use User\Service\DocumentManagerAwareInterface;
use User\Document\Profile;

class RegisterController extends AbstractActionController implements DocumentManagerAwareInterface
{

    protected $dm;


    public function setDocumentManager(DocumentManager $dm)
    {

        $this->dm = $dm;

    }


    public function indexAction()
    {

        $form = new RegisterForm();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $form->setData($request->getPost());

            if ($form->isValid()) {

                $data = $form->getData();
                $userManager = new UserManager();
                $user = $userManager->addUser($data);

                //Set the profile
                $profile = new Profile();

                $user->setProfile($profile);

                $this->dm->persist($user);

                $this->dm->flush();

                $this->flashMessenger()->addSuccessMessage('You have successfully registered! Please sign in to your account.');


                return $this->redirect()->toRoute('user_login');
            }
        }

        return array(
            'form' => $form
        );

    }

}