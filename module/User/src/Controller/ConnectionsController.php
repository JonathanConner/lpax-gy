<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Service\DocumentManagerAwareInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use User\Document\Relationship\Friendship;
use User\Document\Message\Notification;
use User\Document\Relationship\Connection;
use Doctrine\ODM\MongoDB\MongoDBException;
use Zend\View\Model\JsonModel;
use User\Document\User;

/**
 * ConnectionsController
 *
 * @author
 *
 * @version
 *
 */
class ConnectionsController extends AbstractActionController implements DocumentManagerAwareInterface
{

    protected $dm;


    public function setDocumentManager(DocumentManager $dm)
    {

        $this->dm = $dm;

    }


    public function indexAction()
    {

        $user = $this->identity();

        $connections = $user->getConnections();

        $view = new ViewModel([
            'connections' => $connections
        ]);
        $view->setTemplate('dashboard/connections/index');

        return $this->dashboardBuilder($view, $this->layout());

    }


    public function createAction()
    {

        $user = $this->identity();
        $request = $this->getRequest();

        if ($request->isPost()) {

            $data = $request->getPost();

            if (isset($data['uid'])) {

                $id = $data['uid'];

                $friend = $this->dm->getRepository('User\\Document\\User')->findOneBy(array(
                    'id' => $id
                ));

                // If no User was found flashMessage and Redirect
                if (! $friend) {
                    $this->flashMessenger()->addErrorMessage("Your request failed. Invalid User ID!");
                    return $this->redirect()->toRoute('user_dashboard/connections');
                }

                $connection = new Connection($user);

                $system = $this->dm->getRepository('User\\Document\\User')->findOneBy(array(
                    'userName' => 'System'
                ));

                $notification = new Notification($system, "New relationship verification.", $user->getFullName() . " has sent you a relationship request that needs your verification.
                    <a href='/user/dashboard/connection/verify/" . $connection->getId() . "'>");

                $notification->setRecipients($friend);

                try {

                    $this->dm->persist($connection);
                    $this->dm->persist($user);
                    $this->dm->persist($notification);
                    $this->dm->persist($friend);

                    $this->dm->flush();

                } catch (MongoDBException $e) {

                    $this->flashMessenger()->addErrorMessage("Your request failed. Error: " . $e->getCode());
                    return $this->redirect()->toRoute('user_dashboard/connections');

                }

                $this->flashMessenger()->addSuccessMessage("A connection request was sent. Now all you have to do is wait for verification!");
                return $this->redirect()->toRoute('user_dashboard/connections');

            }

        }

    }


    public function searchUsersAction()
    {

        if($this->getRequest()->isGet()){
            $query = $this->params()->fromQuery();
            if($query){
                $users = $this->dm->createQueryBuilder('User')
                    ->find(
                        [
                            '$or'=>[
                                [
                                    'firstName' => new \MongoRegex("/.*{$query['user']}.*/i"),
                                ],
                                [
                                    'lastName' => new \MongoRegex("/.*{$query['user']}.*/i")
                                ]
                            ]

                        ]
                    )
                    ->getQuery()
                    ->execute();
                $data = [];
                foreach($users as $user)
                {
                    $data[]['id'] = $user['id'];
                    $data[]['firstName'] = $user['firstName'];
                    $data[]['lastName']= $user['firstName'];
                }
                return new JsonModel($data);
            }
        }
    }

}