<?php

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

/**
 * AlbumController
 *
 * @author
 *
 * @version
 *
 */
class AlbumController extends AbstractActionController implements \User\Service\DocumentManagerAwareInterface
{

    protected $dm;

    public function setDocumentManager(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function indexAction()
    {

        $user = $this->identity();

        $albums = $user->getAlbums();

        $view = new ViewModel(array('users'=>$users));
		$view->setTemplate('dashboard/index');

		return $this->dashboardBuilder($view, $this->layout());



    }

}