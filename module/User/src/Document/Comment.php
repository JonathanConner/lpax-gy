<?php
namespace User\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\EmbeddedDocument
 */
class Comment extends DomainObject
{

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\ReferenceOne(targetDocument="User")
     */
    protected $author;


    /**
     * @ODM\Date
     */
    protected $date;

    /**
     * @ODM\Field(type="string")
     */
    protected $text;

    /**
     * @ODM\EmbedMany(targetDocument="Comment")
     */
    protected $replies = [];



    public function __construct(User $user, $t){

        $this->date = new \DateTime();
        $this->text = $t;

        $this->author = $user;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getId()
    {

        return $this->id;

    }


    /**
     *
     * @param unknown_type $id
     */
    public function setId($id)
    {

        $this->id = $id;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getAuthor()
    {

        return $this->author;

    }


    /**
     *
     * @param unknown_type $author
     */
    public function setAuthor($author)
    {

        $this->author = $author;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getText()
    {

        return $this->text;

    }


    /**
     *
     * @param unknown_type $text
     */
    public function setText($text)
    {

        $this->text = $text;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getReplies()
    {

        return $this->replies;

    }


    /**
     *
     * @param unknown_type $replies
     */
    public function addReplies($reply)
    {

        $this->replies[] = $reply;

    }

}