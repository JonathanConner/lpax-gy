<?php
namespace User\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;

/**
 * @ODM\Document(repositoryClass="User\Document\Repository\PostRepository")
 */
class Post extends DomainObject
{

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Date
     */
    protected $created;

    /**
     * @ODM\Field(type="string")
     */
    protected $title;

    /**
     * @ODM\Field(type="string")
     */
    protected $body;

    /**
     * @ODM\EmbedMany(targetDocument="Comment")
     */
    protected $comments = array();

    /**
     * @ODM\ReferenceOne(targetDocument="User")
     */
    protected $author;


    public function __construct(User $user = null, $title = null, $body=null)
    {

        $this->created = new \DateTime();
        if(isset($user)) {
            $this->author = $user;
            $user->addPost($this);
        }
        if(isset($title)) $this->title = $title;
        if(isset($body)) $this->body = $body;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getId()
    {

        return $this->id;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getCreated()
    {

        return $this->created->format("m/d/Y @ h:i A");

    }


    /**
     *
     * @param unknown_type $created
     */
    public function setCreated($created)
    {

        $this->created = $created;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getTitle()
    {

        return $this->title;

    }


    /**
     *
     * @param unknown_type $title
     */
    public function setTitle($title)
    {

        $this->title = $title;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getBody()
    {

        return $this->body;

    }


    /**
     *
     * @param unknown_type $body
     */
    public function setBody($body)
    {

        $this->body = $body;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getComments()
    {

        return $this->comments;

    }


    /**
     *
     * @param unknown_type $comments
     */
    public function addComment(Comment $comments)
    {

        $this->comments[] = $comments;

    }


    /**
     *
     * @return the unknown_type
     */
    public function getAuthor()
    {

        return $this->author;

    }


    /**
     *
     * @param unknown_type $author
     */
    public function setAuthor($author)
    {

        $this->author = $author;

    }

}
