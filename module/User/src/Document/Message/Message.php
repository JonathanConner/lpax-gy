<?php

namespace User\Document\Message;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\ArrayCollection;
use User\Document\User;
use User\Document\DomainObject;
use User\Document\DocumentSerializer;

/**
 * @ODM\Document
 * @ODM\InheritanceType("SINGLE_COLLECTION")
 * @ODM\DiscriminatorField("type")
 * @ODM\DiscriminatorMap({"notification"="Notification", "message"="Message"})
 */
class Message extends DomainObject
{

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Date
     */
    protected $created;

    /**
     * @ODM\Date
     */
    protected $sent;

    /**
     * @ODM\ReferenceOne(targetDocument="User\Document\User")
     */
    protected $author;

    /**
     * @ODM\ReferenceMany(targetDocument="User\Document\User")
     */
    protected $recipients;

    /**
     * @ODM\Field(type="string")
     */
    protected $subject;

    /**
     * @ODM\Field(type="string")
     */
    protected $body;

    /**
     * @ODM\Field(type="string")
     */
    protected $status = "unread";

    /**
     * @ODM\Integer
     */
    protected $priority;




    public function __construct(User $author)
    {

        $this->created = new \DateTime();
        $this->setAuthor($author);
        $this->recipients = new ArrayCollection();


    }


    /**
     *
     * @return ODM\Id
     */
    public function getId()
    {

        return $this->id;

    }


    /**
     *
     * @return string
     */
    public function getCreated()
    {

        return $this->created->format("m/d/Y h:i A");

    }


    /**
     *
     * @return \DateTime
     */
    public function getSent()
    {

        return $this->sent;

    }


    /**
     *
     * @param \DateTime
     */
    public function setSent($sent)
    {

        $this->sent = $sent;

    }


    /**
     *
     * @param User $author
     */
    public function setAuthor(User $author)
    {

        $this->author = $author;
        $author->addMessage($this);

    }


    /**
     *
     * @return User
     */
    public function getAuthor()
    {

        return $this->author;

    }


    /**
     *
     * @param User $recipient
     */
    public function setRecipients(User $recipient=null)
    {

        $this->recipients[]=$recipient;
        $recipient->addMessage($this);

    }


    /**
     *
     * @return array
     */
    public function getRecipients()
    {

        return $this->recipients->toArray();

    }


    /**
     *
     * @param string $body
     */
    public function setBody($body)
    {

        $this->body = $body;

    }


    /**
     * @return string
     */
    public function getBody()
    {

        return $this->body;

    }


    /**
     *
     * @return string unknown_type
     */
    public function getSubject()
    {

        return $this->subject;

    }


    /**
     *
     * @param string $subject
     */
    public function setSubject($subject)
    {

        $this->subject = $subject;

    }


    /**
     *
     * @return string unknown_type
     */
    public function getStatus()
    {

        return $this->status;

    }


    /**
     *
     * @param string $status
     */
    public function setStatus($status)
    {

        $this->status = $status;

    }

}