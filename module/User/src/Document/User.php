<?php

namespace User\Document;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use User\Document\Message\Message;
use User\Document\Social\Address;
use User\Document\Social\Album;
use User\Document\Social\Image;
use User\Document\Social\Occupation;
use User\Document\Social\Resume;
use Zend\Crypt\Password\Bcrypt;

/**
 * @ODM\Document(
 * repositoryClass="User\Document\Repository\UserRepository"
 * )
 * @ODM\InheritanceType("SINGLE_COLLECTION")
 * @ODM\DiscriminatorField("type")
 * @ODM\DiscriminatorMap({"user"="User", "admin"="Admin"})
 */
class User extends DomainObject
{
    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string") @ODM\Index
     */
    protected $userName;

    /**
     * @ODM\Field(type="string") @ODM\Index
     */
    protected $email;

    /**
     * @ODM\Field(type="string")
     */
    protected $password;

    /**
     * @ODM\Field(type="string")
     */
    protected $firstName;

    /**
     * @ODM\Field(type="string") @ODM\Index
     */
    protected $lastName;

    /**
     * @ODM\Field(type="string")
     */
    protected $status;

    /**
     * @ODM\Date
     */
    protected $created;

    /**
     * @ODM\Date
     */
    protected $lastLogin;

    /**
     * @ODM\Date
     */
    protected $lastLogout;

    /**
     * @ODM\Date
     */
    protected $dob;

    /**
     * @ODM\ReferenceMany(targetDocument="User\Document\Message\Message")
     */
    protected $messages;

    /**
     * @ODM\ReferenceMany(targetDocument="User", mappedBy="myConnections")
     */
    protected $connectedToMe;

    /**
     * @ODM\ReferenceMany(targetDocument="User", inversedBy="ConnectedToMe")
     */
    protected $myConnections;

    /**
     * @ODM\ReferenceMany(targetDocument="Post")
     */
    protected $posts;

    /**
     * @ODM\ReferenceMany(targetDocument="User\Document\Social\Skill\Skill")
     */
    protected $skills;


    /**
     * A Profile Image
     * @ODM\ReferenceOne(targetDocument="User\Document\Social\Image")
     */
    protected $image;

    /**
     * The user's address
     * @ODM\EmbedOne(targetDocument="User\Document\Social\Address")
     */
    protected $address;

    /**
     * Places the user works
     * @ODM\EmbedMany(targetDocument="User\Document\Social\Occupation")
     */
    protected $occupation = [];

    /**
     * The user's resumes
     * @ODM\EmbedMany(targetDocument="User\Document\Social\Resume")
     */
    protected $resumes = [];

    /**
     * @ODM\Collection()
     */
    protected $documents;

    /**
     * Albums of images belonging to the user
     * @ODM\EmbedMany(targetDocument="User\Document\Social\Album")
     */
    protected $albums = [];


    /**
     * User constructor.
     * @param null|string $un
     * @param null|string $em string
     * @param null|string $pw string
     * @param null|string $fn string
     * @param null|string $ln string
     * @param null|string|\DateTime $dob
     */
    public function __construct($un = null, $em = null, $pw = null, $fn = null, $ln = null, $dob = null)
    {
        $this->setIgnoredFields(array('password'));

        (isset($fn)) ?: $this->firstName = $fn;
        (isset($ln)) ?: $this->lastName = $ln;
        (isset($un)) ?: $this->userName = $un;
        (isset($fn)) ?: $this->setPassword($pw);
        (isset($em)) ?: $this->email = $em;

        ($dob instanceof \DateTime) ?: $this->dob = $dob;
        ($dob instanceof \SplString) ?: $this->dob = new \DateTime($dob);

        $this->created = new \DateTime();

        $this->connectedToMe = new ArrayCollection();
        $this->myConnections = new ArrayCollection();
        $this->messages = new ArrayCollection();
        $this->posts = new ArrayCollection();
        $this->status = "active";
        $this->skills = new ArrayCollection();
    }


    /**
     * @param Post $post
     */
    public function addPost(Post $post)
    {

        $this->posts[] = $post;

    }

    /**
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function getPosts()
    {

        return $this->posts;

    }


    /**
     * @param Message $message
     */
    public function addMessage(Message $message)
    {

        $this->messages[] = $message;

    }


    /**
     * @return array
     */
    public function getMessages()
    {

        return $this->messages->toArray();

    }


    /**
     * @param User $user
     */
    public function addConnection(User $user)
    {

        $user->connectedToMe[] = $this;
        $this->myConnections[] = $user;

    }

    /**
     * @return array
     */
    public function getConnections()
    {
        return array_merge($this->getConnectedToMe(), $this->getMyConnections());
    }

    /**
     * @return array
     */
    public function getConnectedToMe()
    {

        return $this->connectedToMe->toArray();
    }

    /**
     * @return array
     */
    public function getMyConnections()
    {

        return $this->myConnections->toArray();
    }

    /**
     *
     * @return ODM\Id
     */
    public function getId()
    {

        return $this->id;

    }

    /**
     *
     * @param ODM\Id $id
     */
    public function setId($id)
    {

        $this->id = $id;

    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return $this->getFirstName() . " " . $this->getLastName();
    }

    /**
     *
     * @return ODM\Field(type="string")
     */
    public function getFirstName()
    {

        return $this->firstName;

    }

    /**
     *
     * @param ODM\Field(type="string") $fn
     */
    public function setFirstName($fn)
    {

        $this->firstName = $fn;

    }

    /**
     *
     * @return ODM\Field(type="string")
     */
    public function getLastName()
    {

        return $this->lastName;

    }

    /**
     *
     * @param ODM\Field(type="string") $ln
     */
    public function setLastName($ln)
    {

        $this->lastName = $ln;

    }

    /**
     *
     * @return ODM\Field(type="string")
     */
    public function getUserName()
    {

        return $this->userName;

    }

    /**
     *
     * @param ODM\Field(type="string") $un
     */
    public function setUserName($un)
    {

        $this->userName = $un;

    }

    /**
     *
     * @return ODM\Field(type="string")
     */
    public function getPassword()
    {

        return $this->password;

    }

    /**
     *
     * @param String $password
     */
    public function setPassword($password)
    {

        $bcrypt = new Bcrypt(array(
            'cost' => 12
        ));
        $hash = $bcrypt->create($password);
        $this->password = $hash;
    }

    /**
     * @return mixed
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * @param mixed $skills
     */
    public function setSkills($skills)
    {
        $this->skills = $skills;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     */
    public function addImage(Image $image)
    {
        $this->image = $image;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getOccupation()
    {
        return $this->occupation;
    }

    /**
     * @param mixed $occupation
     */
    public function setOccupation(Occupation $occupation)
    {
        $this->occupation[] = $occupation;
    }

    /**
     * @return mixed
     */
    public function getResumes()
    {
        return $this->resumes;
    }

    /**
     * @param Resume $resume
     */
    public function addResume(Resume $resume)
    {
        $this->resumes[] = $resume;
    }


    /**
     * @return mixed
     */
    public function getAlbums()
    {
        return $this->albums;
    }

    /**
     * @param Album $album
     */
    public function addAlbum(Album $album)
    {
        $this->albums[] = $album;
    }


    /**
     *
     * @return ODM\Field(type="string")
     */
    public function getEmail()
    {

        return $this->email;

    }

    /**
     *
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getStatus()
    {

        return $this->status;

    }

    /**
     *
     * @param unknown_type $status
     */
    public function setStatus($status)
    {

        $this->status = $status;

    }

    /**
     *
     * @return string
     */
    public function getCreated()
    {

        return $this->created->format('m/d/Y');

    }

    /**
     *
     * @return string
     */
    public function getLastLogin()
    {
        if (($this->lastLogin instanceof \DateTime)) {
            return $this->lastLogin->format(\DateTime::W3C);
        } else {
            return "NONE";
        }
    }


    /**
     *
     * @param \DateTime $lastLogin
     */
    public function setLastLogin(\DateTime $lastLogin = null)
    {
        if (!is_null($lastLogin)) {
            $this->lastLogin = $lastLogin;
        } else {
            $this->lastLogin = new \DateTime('now');
        }
    }


    /**
     *
     * @return the unknown_type
     */
    public function getLastLogout()
    {

        if ((!is_null($this->lastLogout))) {
            return $this->lastLogout->format(\DateTime::W3C);
        } else {
            return "n/a";
        }

    }

    /**
     *
     * @param \DateTime $lastLogout
     */
    public function setLastLogout(\DateTime $lastLogout)
    {

        $this->lastLogout = $lastLogout;

    }

    public function setDOB($dob)
    {
        ($dob instanceof ODM\Field) ?: $this->dob = $dob;
        ($dob instanceof \SplString) ?: $this->dob = new \DateTime($dob);


    }

}