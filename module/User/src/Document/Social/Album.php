<?php

namespace User\Document\Social;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\ArrayCollection;
use User\Document\DomainObject;
use User\Document\User;

/**
 * @ODM\EmbeddedDocument
 */
class Album extends DomainObject{

    /**
     * @ODM\Id
     */
    private $id;

    /**
     * @ODM\Field(type="string")
     */
    private $name;

    /**
     * @ODM\ReferenceOne(targetDocument="User\Document\User")
     */
    private $owner;



    /**
     * @ODM\ReferenceMany(
     *      discriminatorMap={
     *              "video"="Video",
     *              "image"="Image"
     *      }
     * )
     */
    private $media;


    public function __construct($name){
        $this->name = $name;
        $this->media = new ArrayCollection();
    }
    public function getId(){
        return $this->id;
    }

    public function setName($name){
        $this->name = $name;
    }

    public function getName(){
        return $this->name;
    }

    public function addMedia($media){
        $this->media[]=$media;
    }

    public function setOwner(User $owner){
        $this->owner = $owner;
    }

}