<?php

namespace User\Document\Social;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use User\Document\DomainObject;

/**
 * @ODM\Document
 */
class Image extends DomainObject {

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field
     */
    protected $name;

    /**
     * @ODM\File
     */
    protected $file;

    /**
     * @ODM\Field
     */
    protected $uploadDate;

    /**
     * @ODM\Field
     */
    protected $length;

    /**
     * @ODM\Field
     */
    protected $chunkSize;

    /**
     * @ODM\Field
     */
    protected $md5;

    public function __construct($id){
        $this->id = $id;
    }

    public function getId() {

        return $this->id;

    }


    public function setName( $name ) {

        $this->name = $name;

    }


    public function getName() {

        return $this->name;

    }


    public function getFile() {

        return $this->file;

    }


    public function setFile( $file ) {

        $this->file = $file;

    }

}