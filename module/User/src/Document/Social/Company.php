<?php
/**
 *
 */
namespace User\Document\Social;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use User\Document\DomainObject;

/**
 * @ODM\Document
 */
class Company extends DomainObject
{

	/**
	 * @ODM\Id
	 */
	protected $id;

	/**
	 * The official company name
	 * @ODM\Field(type="string") @ODM\Index
	 */
	protected $name;

	/**
	 * The official type
	 * @ODM\Field(type="string")
	 */
	protected $type;

	/**
	 * @ODM\Field(type="string")
	 */
	protected $purpose;

	/**
	 * @ODM\Field(type="string")
	 */
	protected $website;

	/**
	 * @ODM\EmbedMany("Address")
	 */
	protected $locations = [];

	/**
	 * @ODM\ReferenceMany(targetDocument="User\Document\User")
	 */
	protected $employees;

	/**
	 * @ODM\Collection
	 */
	protected $socialProfiles = [];

	public function __construct(User $user, $name, $type, $purpose, $locations, $employees = null)
	{
		
		$this->employees = new ArrayCollection();
		$this->employees->add($user);
		
		if (isset($employees)) {
			$this->employees->add($employees);
		}
		
		$this->name = $name;
		$this->type = $type;
		$this->purpose = $purpose;
	
	}

}