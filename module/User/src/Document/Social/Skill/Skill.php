<?php

namespace User\Document\Social\Skill;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use User\Document\DomainObject;

/**
 * @ODM\Document
 */
class Skill extends DomainObject
{

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $name;

    /**
     * @ODM\Field(type="string")
     */
    protected $description;

    /**
     * @ODM\Date
     */
    protected $since;

    public function __construct()
    {

    }

}