<?php

namespace User\Document\Social\Rank;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use User\Document\DomainObject;

/**
 * @ODM\Document
 */
class Rank extends DomainObject
{

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * @ODM\Field(type="string")
     */
    protected $name;

    /**
     * @ODM\Document()
     */
    protected $desc;


    /**
     * @return mixed
     */
    public function getName()
    {

        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {

        $this->name = $name;
    }


    public function __construct()
    {

    }

}