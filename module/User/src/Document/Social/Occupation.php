<?php
namespace User\Document\Social;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use User\Document\DomainObject;
use Zend\Stdlib\DateTime;

/**
 * @ODM\EmbeddedDocument
 */
class Occupation extends DomainObject
{

    /**
     * @ODM\Id
     */
    protected $id;

    /**
     * The official company name
     *
     * @ODM\Field(type="string") @ODM\Index
     */
    protected $companyName;

    /**
     * The official title held by the user
     *
     * @ODM\Field(type="string")
     */
    protected $officialTitle;

    /**
     * Employment start date
     *
     * @ODM\Date
     */
    protected $employedSince;

    /**
     * Employment termination date
     *
     * @ODM\Date
     */
    protected $dateLeft;

    /**
     * Occupation constructor.
     * @param $companyName
     * @param $officialTitle
     * @param \DateTime $employedSince
     * @param \DateTime $dateLeft
     */
    public function __construct($companyName, $officialTitle, \DateTime $employedSince, \DateTime $dateLeft = null)
    {
        $this->companyName = $companyName;
        $this->officialTitle = $officialTitle;
        $this->employedSince = $employedSince;
        $this->dateLeft = $dateLeft;
    }

    /**
     * @return mixed
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @param mixed $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;
    }

    /**
     * @return mixed
     */
    public function getOfficialTitle()
    {
        return $this->officialTitle;
    }

    /**
     * @param mixed $officialTitle
     */
    public function setOfficialTitle($officialTitle)
    {
        $this->officialTitle = $officialTitle;
    }

    /**
     * @return mixed
     */
    public function getEmployedSince()
    {
        return $this->employedSince->format('M d Y');
    }

    /**
     * @param mixed $employedSince
     */
    public function setEmployedSince($employedSince)
    {
        $this->employedSince = $employedSince;
    }

    /**
     * @return mixed
     */
    public function getDateLeft()
    {
        return $this->dateLeft->format('M d Y');
    }

    /**
     * @param mixed $dateLeft
     */
    public function setDateLeft($dateLeft)
    {
        $this->dateLeft = $dateLeft;
    }


}