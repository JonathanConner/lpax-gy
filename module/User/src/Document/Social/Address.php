<?php


namespace User\Document\Social;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use User\Document\DomainObject;

/**
 * @ODM\EmbeddedDocument
 */
class Address extends DomainObject
{

    /**
     * @ODM\Id
     */
    protected $id;


    /**
     * @ODM\Field(type="string")
     */
    protected $city;

    /**
     * @ODM\Field(type="string")
     */
    protected $state;

    /**
     * @ODM\Integer
     */
    protected $zip;

    /**
     * @ODM\Field(type="string")
     */
    protected $street;

	public function __construct($city, $state, $zip, $street){

	    $this->city = $city;
	    $this->state = $state;
	    $this->zip = $zip;
	    $this->street = $street;


	}


    public function getId()
    {

        return $this->id;

    }


    public function setId($id)
    {

        $this->id = $id;
    }


    public function getCity()
    {

        return $this->city;

    }


    public function setCity($city)
    {

        $this->city = $city;

    }


    public function getState()
    {

        return $this->state;

    }


    public function setState($state)
    {

        $this->state = $state;

    }


    public function getZip()
    {

        return $this->zip;

    }


    public function setZip($zip)
    {

        $this->zip = $zip;

    }


    public function getStreet()
    {

        return $this->street;

    }


    public function setStreet($street)
    {

        $this->street = $street;

    }


}