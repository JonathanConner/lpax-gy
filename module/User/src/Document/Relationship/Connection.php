<?php

namespace User\Document\Relationship;

use Doctrine\ODM\MongoDB\Mapping\Annotations as ODM;
use Doctrine\Common\Collections\ArrayCollection;
use User\Document\User;
use User\Document\DomainObject;
use User\Document\DocumentSerializer;

/**
 * @ODM\Document
 */
class Connection{

    use DocumentSerializer;
    /**
     * @ODM\Id
     */
    protected $id;

	/**
	 * @ODM\Date
	 */
	protected $since;

	/**
	 * @ODM\ReferenceMany(targetDocument="User\Document\User")
	 */
	protected $users;

	/**
	 * @ODM\Boolean
	 */
	protected $verified = false;

	/**
	 * @ODM\Boolean
	 */
	protected $sent = false;

	/**
	 * @ODM\Date
	 */
    protected $lastContact;



	public function __construct( User $creator) {
		$this->users = new ArrayCollection();
		$this->addConnectedUser($creator);
		$creator->addConnection($this);
		$this->setSent(true);
	}


	public function addConnectedUser(User $user ) {

		$this->users->add($user);
	}


	public function setVerified( User $target, $ver ) {
		if( $ver == true ) {
			$this->verified = $ver;
			$this->addConnectedUser($target);
			$target->addConnection($this);
			$this->since = new \DateTime();
		}
	}


	public function getId() {
		return $this->id;
	}


	public function getConnectedUsers() {
		return $this->users->toArray();
	}


	public function setSent( $sent ) {
		$this->sent = $sent;
	}


	public function getSent() {
		return $this->sent;
	}


	public function getVerified() {
		return $this->verified;
	}


    public function getSince()
    {

        return $this->since->format('m/d/Y');

    }


    public function getLastContact()
    {

        return $this->lastContact;

    }


    public function setLastContact($lastContact)
    {

        $this->lastContact = $lastContact;

    }




}