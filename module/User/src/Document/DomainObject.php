<?php
namespace User\Document;

use Doctrine\Common\NotifyPropertyChanged, Doctrine\Common\PropertyChangedListener;
use Doctrine\Common\Collections\ArrayCollection;

abstract class DomainObject implements \ArrayAccess
{
    use DocumentSerializer;
    /*
     * (non-PHPdoc)
     * @see ArrayAccess::offsetExists()
     */
    public function offsetExists($offset)
    {
        $field = ucfirst($offset);
        // In this example we say that exists means it is not null
        $value = $this->{"get$field"}();
        return $value !== null;
    }

    /*
     * (non-PHPdoc)
     * @see ArrayAccess::offsetGet()
     */
    public function offsetGet($offset)
    {
        $field = ucfirst($offset);
        // In this example we say that exists means it is not null
        $value = $this->{"get$field"}();
        if ($value instanceof ArrayCollection) {
            return $value->toArray();
        } else {
            return $value;
        }
    }

    /*
     * (non-PHPdoc)
     * @see ArrayAccess::offsetSet()
     */
    public function offsetSet($offset, $value)
    {
        $field = ucfirst($offset);
        $this->{"set$field"}($value);
    }

    /*
     * (non-PHPdoc)
     * @see ArrayAccess::offsetUnset()
     */
    public function offsetUnset($offset)
    {
        throw new BadMethodCallException("Cannot unset value in for field. Use the DocumentManager!");
    }
}