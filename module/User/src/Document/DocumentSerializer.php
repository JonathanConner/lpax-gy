<?php
/**
 * Created by PhpStorm.
 * Author: Anis Ahmad <anisniit@gmail.com>
 * Date: 5/11/14
 * Time: 10:44 PM
 */
namespace User\Document;

use Doctrine\Common\Collections\ArrayCollection;
use DoctrineModuleTest\Form\Element\ProxyTest;

trait DocumentSerializer
{

    private $_ignoreFields = ['password'];

    /**
     * Convert Doctrine\ODM Document to Array
     *
     * @return array
     */
    function toArr()
    {
        $document = $this->toStdClass();
        return get_object_vars($document);
    }

    /**
     * Convert Doctrine\ODM Document to plain simple stdClass
     *
     * @return \stdClass
     */
    function toStdClass()
    {
        $document = new \stdClass();

        foreach ($this->findGetters() as $getter) {
            $prop = lcfirst(substr($getter, 3));

            if (!in_array($prop, $this->_ignoreFields)) {
                $value = $this->$getter();
                $document->$prop = $this->formatValue($value);
            }
        }

        return $document;
    }

    private function findGetters()
    {
        $funcs = get_class_methods(get_class($this));
        $getters = [];

        foreach ($funcs as $func) {
            if (strpos($func, 'get') === 0) {
                $getters[] = $func;
            }
        }

        return $getters;
    }

    private function formatValue($value)
    {

        if(is_scalar($value)) {

            return $value;

            // If the object uses this trait

        } elseif (is_a($value, 'Doctrine\ODM\MongoDB\PersistentCollection')) {

            $prop = [];

            foreach ($value as $k => $v) {
                $prop[ $k ] = $this->formatValue($v);
            }

            return $prop;

            // If it's a Date, convert to unix timestamp

        } elseif (is_a($value, 'Date')) {

            return $value->getTimestamp();

            // Otherwise leave a note that this type is not formatted
            // So that I can add formatting for this missed class

        } elseif (is_array($value)) {

            return $value;

        } elseif (in_array(__TRAIT__, class_uses(get_class($value))) && method_exists($value, 'toStdClass')) {

            return $value->toStdClass();

            // If it's a collection, format each value

        }elseif($value instanceof ArrayCollection) {

            $a = $value->toArray();
            foreach ($a as $obj){
                if(is_object($obj)){
                    return $obj->toStdClass();
                }
            }
        }

    }

    /**
     * Convert Doctrine\ODM Document to Array
     *
     * @return string
     */
    function toJSON()
    {
        $document = $this->toStdClass();
        return json_encode($document);
    }

    /**
     * Set properties to ignore when serializing
     *
     * @example  $this->setIgnoredFields(array('createdDate', 'secretFlag'));
     *
     * @param array $fields
     */
    function setIgnoredFields(array $fields)
    {
        $this->_ignoreFields = $fields;
    }
}