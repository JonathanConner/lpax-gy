<?php

namespace User;

use User\Document\Relationship\Connection;
use Zend\Authentication\AuthenticationService;
use Zend\ModuleManager\ModuleManager;
use Zend\EventManager\EventInterface as Event;
use User\Document\Event\NotificationEvent;
use Doctrine\ODM\MongoDB\Events;
use Zend\Router\RouteMatch;
use Zend\Mvc\MvcEvent;
use Zend\Stdlib\ResponseInterface;

class Module
{

    protected $whitelist = array(
        'user_login',
        'home',
        'user_register'

    );


    public function onBootstrap(MvcEvent $e)
    {

        $app = $e->getApplication();
        $em = $app->getEventManager();
        $sm = $app->getServiceManager();

        $list = $this->whitelist;

        $auth = $sm->get('Zend\Authentication\AuthenticationService');

        $em->attach(MvcEvent::EVENT_ROUTE, function (MvcEvent $e) use($list, $auth)
        {
            $match = $e->getRouteMatch();

            // No route match, this is a 404
            if (! $match instanceof RouteMatch) {
                return ;
            }

            // Route is whitelisted
            $name = $match->getMatchedRouteName();
            if (in_array($name, $list)) {
                return;
            }

            // User is authenticated
            if ($auth->hasIdentity()) {
                return;
            }

            // Redirect to the user login page, as an example
            $router = $e->getRouter();
            $url = $router->assemble(array(), array(
                'name' => 'user_login'
            ));
            /**
             * @var $response ResponseInterface
             */
            $response = $e->getResponse();
            $response->getHeaders()->addHeaderLine('Location', $url);
            $response->setStatusCode(302);
            return $response;
        }, -1);

    }


    public function getConfig()
    {
        return include __DIR__ . '/../config/module.config.php';
    }


    public function getServiceConfig()
    {

        return array(
            'factories' => array(
                'Zend\Authentication\AuthenticationService' => function ($serviceManager)
                {
                    return $serviceManager->get('doctrine.authenticationservice.odm_default');
                }
            )
        );

    }
    public function getAutoloaderConfig()
    {

        return array(
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' .__NAMESPACE__
                )
            ),
        );

    }

}
