<?php

namespace User\Service;


use User\Document\User;
use Zend\Crypt\Password\Bcrypt;

class UserManager
{

    /**
     * This method adds a new user.
     */
    public function addUser($data)
    {
        // Do not allow several users with the same email address.
        if($this->checkUserExists($data['email'])) {
            throw new \Exception("User with email address " .
                $data['$email'] . " already exists");
        }

        // Create new User entity.
        $user = new User();
        $user->setEmail($data['email']);
        $user->setUserName($data['userName']);
        $user->setFirstName($data['firstName']);
        $user->setLastName($data['lastName']);

        // Encrypt password and store the password in encrypted state.
        $user->setPassword($data['password']);

        $currentDate = date('Y-m-d H:i:s');
        $user->setDateCreated($currentDate);

        $user->setDOB($data['dob']);

        // Add the entity to the entity manager.
        $this->entityManager->persist($user);

        // Apply changes to database.
        $this->entityManager->flush();

        return $user;
    }


}