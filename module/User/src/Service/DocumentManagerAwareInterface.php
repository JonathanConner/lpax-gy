<?php

namespace User\Service;

use Doctrine\ODM\MongoDB\DocumentManager;

/**
 *
 * This interface marks controllers who
 * need to access the document manager.
 *
 */
interface DocumentManagerAwareInterface {

    /**
     * sets the DocumentManager instance
     *
     * @param DocumentManager $dm
     */
    public function setDocumentManager( DocumentManager $dm );

}