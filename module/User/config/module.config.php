<?php
namespace User;

use User\Controller\ConnectionsController;
use User\Controller\DashboardController;
use User\Controller\IndexController;
use User\Controller\LoginController;
use User\Controller\LogoutController;
use User\Controller\MessagesController;
use User\Controller\PostController;
use User\Controller\ProfileController;
use User\Controller\RegisterController;
use User\Document\User;
use User\Service\DocumentManagerAwareInterface;
use Zend\Crypt\Password\Bcrypt;
use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;

return [
    'router' => [
        'routes' => [
            'user' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/user',
                    'defaults' => [
                        'controller' => IndexController::class,
                        'action' => 'index',
                    ],
                ],
            ],
            'user_logout' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/user/logout',
                    'defaults' => [
                        'controller' => LogoutController::class,
                        'action' => 'index',
                    ],
                ]
            ],
            'user_register' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/user/register',
                    'defaults' => [
                        'controller' => RegisterController::class,
                        'action' => 'index',
                    ],
                ]
            ],
            'user_login' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/user/login',
                    'defaults' => [
                        'controller' => LoginController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'default' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/[:controller[/:action]]',
                            'constraints' => [
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ],
                            'defaults' => []
                        ]
                    ]
                ]
            ], // ------user_login
            'user_dashboard' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/user/dashboard',
                    'defaults' => [
                        'controller' => DashboardController::class,
                        'action' => 'index'
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'connections' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/connections[/:action[/:id]]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'id' => '[a-z0-9]*',
                            ],
                            'defaults' => [
                                'controller' => ConnectionsController::class,
                                'action' => 'index'
                            ]
                        ]
                    ],
                    'messages' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/messages[/:action]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ],
                            'defaults' => [
                                'controller' => MessagesController::class,
                                'action' => 'index'
                            ]
                        ]
                    ],
                    'post' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/post[/:action]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ],
                            'defaults' => [
                                'controller' => PostController::class,
                                'action' => 'index'
                            ]
                        ]
                    ],'profile' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/profile[/:action]',
                            'constraints' => [
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                            ],
                            'defaults' => [
                                'controller' => ProfileController::class,
                                'action' => 'index'
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ], // ------user_dashboard
    // ----router
    'controllers' => [
        'factories' => [
            IndexController::class => InvokableFactory::class,
            RegisterController::class => InvokableFactory::class,
            LoginController::class => InvokableFactory::class,
            LogoutController::class => InvokableFactory::class,
            DashboardController::class => InvokableFactory::class,
            MessagesController::class => InvokableFactory::class,
            ConnectionsController::class => InvokableFactory::class,
            PostController::class => InvokableFactory::class,
            ProfileController::class => InvokableFactory::class,

        ],
        'invokables' => [],
        'initializers' => [
            function ($instance, $sl) {
                if ($instance instanceof DocumentManagerAwareInterface) {
                    $instance->setDocumentManager($sl->getServiceLocator()
                        ->get('doctrine.documentmanager.odm_default'));
                }
            }
        ]
    ],
    'controller_plugins' => [
        'invokables' => [
            'dashboardBuilder' => 'User\Controller\Plugin\DashboardBuilder'
        ]
    ],

    // ------controllers
    'view_manager' => [
        'template_path_stack' => [
            __DIR__ . '/../view',
            __DIR__ . '/../view/user',
            __DIR__ . '/../view/user/dashboard'
        ],
        'template_path' => [
            'dashboard/messages' => __DIR__ . '/../view/user/dashboard/messages.phtml',
            'user/login/index' => __DIR__ . '/../view/user/login/index.phtml',
            'dashboard/index' => __DIR__ . '/../view/user/dashboard/index.phtml',
            'dashboard/view' => __DIR__ . '/../view/user/dashboard/view.phtml',
            'dashboard/sidebar' => __DIR__ . '/../view/user/dashboard/sidebar.phtml',
            'dashboard/post' => __DIR__ . '/../view/user/dashboard/post.phtml',
            'dashboard/connections/index' => __DIR__ . '/../view/user/dashboard/connections/index.phtml'
        ]
    ],
    // ------view_manager
    'doctrine' => [
        'authentication'=>[
            'odm_default' => [
                'object_manager'      => 'doctrine.documentmanager.odm_default',
                'identity_class'      => 'User\Document\User',
                'identity_property'   => 'email',
                'credential_property' => 'password',
                'credential_callable' =>
                    function(User $user, $passwordGiven) {
                        $bcrypt = new Bcrypt();
                        return $bcrypt->verify($passwordGiven, $user->getPassword());
                    },
            ],
        ],
        'driver' => [
            __NAMESPACE__ . '_driver' => [
                'class' => 'Doctrine\ODM\MongoDB\Mapping\Driver\AnnotationDriver',
                'paths' => [
                    __DIR__ . '/../src/User/Document',
                ]
            ],
            'odm_default' => [
                'drivers' => [
                    __NAMESPACE__ . '\Document' => __NAMESPACE__ . '_driver',
                ]
            ]
        ]
    ]
];



