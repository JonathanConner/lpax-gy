<?php
/* namespace UserTest;

use User\Document\Profile;
use User\Document\User;
use DocumentManager\DocumentManagerAwareInterface;
use Doctrine\ODM\MongoDB\DocumentManager;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use User\Document\Relationship\Friendship;
use User\Document\Message\Notification;
use \User\Document\Message\Message;
use Zend\Crypt\Password\Bcrypt;
use User\Document\Admin;
use User\Document\Relationship\Connection;

class DocumentsTest extends AbstractHttpControllerTestCase implements DocumentManagerAwareInterface
{

    protected $dm;

    public function setUp()
    {
        $this->setApplicationConfig(include '/var/www/site/config/application.config.php');
        $this->setDocumentManager($this->getApplicationServiceLocator()
            ->get('doctrine.documentmanager.odm_default'));
        parent::setUp();
    }

    public function setDocumentManager(DocumentManager $dm)
    {
        $this->dm = $dm;
    }

/*     public function testArrayAccessObjects()
    {
        $user = $this->getAUser();

        $this->assertInstanceOf('User\Document\DomainObject', $user);

        $this->assertTrue($user['userName'] === $user->getUserName());
    }
 */
/*     public function testUserPassword()
    {
        $user = $this->getAUser();

        $crypt = new Bcrypt();

        $this->assertTrue($crypt->verify('IDlt6345%!', $user['password']), "Password is not valid!");
    }
 */
/*
    public function testCanCreateUsers(){

        $admin = new \User\Document\User("Wiggins", "jwiggins@example.com","IDlt6345%!", "John", "Wiggins");

        $this->dm->persist($admin);

        $this->dm->flush();

    }
 */
/*     public function testCanCreateConnections()
    {
        $user = $this->dm->getRepository('User\\Document\\User')->findOneBy(array('userName'=>"JonConner"));

        $friend = $this->dm->getRepository('User\Document\User')->findOneBy(array('userName'=>"JackBlack31"));

        $connection = new Connection($user);

        $this->dm->persist($connection);
        $this->dm->persist($user);


        $this->dm->flush();
    }
 */
/*     public function testCanVerifyConnections(){

        $connection = $this->getAUser()->getConnections()->toArray();
        $connection = $this->dm->getRepository('User\Document\Relationship\Connection')->findOneBy(array('id'=>$connection[0]['id']));
        $friend = $this->dm->getRepository('User\Document\User')->findOneBy(array('userName'=>"JackBlack31"));

        $connection->setVerified($friend, true);

        $this->assertTrue($connection->getVerified());

        $this->dm->persist($connection);
        $this->dm->persist($friend);

        $this->dm->flush();

    }
 */
/*
    public function getAUser() {
        return $this->dm->getRepository('User\Document\Admin')->findOneBy(array('userName'=>"JonConner"));
    }
}*/