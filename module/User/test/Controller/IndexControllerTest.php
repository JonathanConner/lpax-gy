<?php
namespace UserTest\Controller;

use Doctrine\ODM\MongoDB\DocumentManager;
use User\Document\User;
use User\Service\DocumentManagerAwareInterface;
use Zend\Stdlib\ArrayUtils;

class IndexControllerTest extends \Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase implements
    DocumentManagerAwareInterface{

    protected $dm;


    public function setUp() {


        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../../config/application.config.php',
            $configOverrides
        ));
        $this->setDocumentManager(
            $this->getApplicationServiceLocator()
                ->get('doctrine.documentmanager.odm_default'));
        parent::setUp();

    }


    public function setDocumentManager( DocumentManager $dm ) {

        $this->dm = $dm;

    }

    public function testIndexActionCanBeAccessed() {

        $this->dispatch('/user');
        $this->assertModuleName('User');
        $this->assertMatchedRouteName('user');

    }

    public function testRedirectToLogin(){
        $this->dispatch('/user');
        $this->assertRedirectTo('/user/login');
    }




}