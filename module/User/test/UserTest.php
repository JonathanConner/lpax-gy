<?php
namespace UserTest;

use Doctrine\ODM\MongoDB\MongoDBException;
use User\Document\Message\Notification;
use User\Document\Post;
use User\Document\Social\Address;
use User\Document\Admin;
use User\Document\Social\Album;
use User\Document\Social\Image;
use User\Document\Social\Occupation;
use User\Document\User;
use User\Service\DocumentManagerAwareInterface;
use Zend\Stdlib\ArrayUtils;
use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
class UserTest extends AbstractHttpControllerTestCase implements DocumentManagerAwareInterface
{

    /**
     * @type \Doctrine\ODM\MongoDB\DocumentManager
     */
    protected $dm;


    public function setUp()
    {

        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            $configOverrides
        ));
        $this->setDocumentManager($this->getApplicationServiceLocator()->get('doctrine.documentmanager.odm_default'));
        parent::setUp();

    }


    /**
     * sets the DocumentManager instance
     *
     * @param \Doctrine\ODM\MongoDB\DocumentManager $dm
     */
    public function setDocumentManager(\Doctrine\ODM\MongoDB\DocumentManager $dm)
    {
        $this->dm = $dm;
    }

    public function testCanCreateUsers()
    {
        $this->userBuilder();
    }

    public function testCanCreatePosts(){

//       $user2 = $this->dm->getRepository('User\Document\User')->findOneBy(array('userName' => 'john.wiggins'));
//        $post = new Post($user2,null, "My first post to this site!");
//        $user2->addPost($post);
//        $this->dm->persist($post);
//        $this->dm->persist($user2);
//        $this->dm->flush();
    }
    public function testCanCreateUserConnection()
    {
/*
        $user1 = $this->dm->getRepository('User\Document\User')->findOneBy(array('userName' => 'jon.conner'));
        $user2 = $this->dm->getRepository('User\Document\User')->findOneBy(array('userName' => 'john.wiggins'));
//
//        $this->assertTrue(($user1 instanceof User) && ($user2 instanceof User));
//
        $user1->addConnection($user2);

        $this->dm->persist($user1);
        $this->dm->persist($user2);


        $this->dm->flush();*/
    }
  /**
     *
     */
    public function userBuilder()
    {



        if(!is_null($this->dm->getRepository('User\Document\User')->findOneBy(array('userName' => 'jon.conner'))))
        {
            $this->dm->createQueryBuilder('User\Document\User')
                ->remove()
                ->field('userName')->equals('jon.conner')
                ->getQuery()
                ->execute();
        }

        $profile = new Admin("jon.conner", "jonathan.g.conner@gmail.com", "IDlt6345%!", "Jonathan", "Conner");

        $address = new Address('Richmond', 'VA', 23832, '8005 Gates Bluff Pl.');
        $profile->setAddress($address);

        $occupation = new Occupation('MWV', 'Intern', new \DateTime('06-04-2015'), new \DateTime('07-10-2015'));
        $profile->setOccupation($occupation);

        $image = new Image(new \MongoId());
        $image->setName('Profile Image');
        $image->setFile('C:\data\downloads\4o4vd1qh9sgne7b6ca59cgmlgp.jpg');


        $album = new Album('default');
        $album->setOwner($profile);
        $album->addMedia($image);
        $profile->addAlbum($album);


        $notif = new Notification($profile);
        $notif->setSubject("Welcome!");
        $notif->setBody("User account has been created!");

        $post = new Post($profile, "Initialize", "This is my first post to this site!");

        $this->dm->persist($post);
        $this->dm->persist($image);
        $this->dm->persist($album);
        $this->dm->persist($notif);
        $this->dm->persist($profile);
        $this->dm->flush();
    }

}