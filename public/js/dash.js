/**
 * @param{{beforeSend: ?function, url: !string, spinner: ?function, dataType: !string, data: !Object, type: ?string,
 *     cache: ?boolean, success: ?function}}
 */
var AjaxHandler = (function ($) {
      return (function (params) {

            //Define default settings
            var settings = $.extend({
                  beforeSend: function () {
                        console.info({
                              "object": $(this).class.className,
                              "state": "CALLTO: beforeSend"
                        });
                  },
                  url: "",
                  spinner: undefined,
                  dataType: "",
                  data: {},
                  type: "GET",
                  cache: false,
                  success: function (response) {
                        console.log(response)
                  }
            }, params);

            $.ajax({
                  beforeSend: settings.beforeSend,
                  url: settings.url,
                  dataType: settings.dataType,
                  type: settings.type,
                  data: settings.data,
                  success: settings.success,
                  complete: function () {
                  }
            });
      });

})(jQuery);

$(function () {
      $()
});
