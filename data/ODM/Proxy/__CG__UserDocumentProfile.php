<?php

namespace ODM\Proxy\__CG__\User\Document;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Profile extends \User\Document\Profile implements \Doctrine\ODM\MongoDB\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Persistence\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Common\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array properties to be lazy loaded, with keys being the property
     *            names and values being their default values
     *
     * @see \Doctrine\Common\Persistence\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = [];



    /**
     * @param \Closure $initializer
     * @param \Closure $cloner
     */
    public function __construct($initializer = null, $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', 'id', 'tagline', 'image', 'address', 'occupation', 'resumes', 'documents', 'albums'];
        }

        return ['__isInitialized__', 'id', 'tagline', 'image', 'address', 'occupation', 'resumes', 'documents', 'albums'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Profile $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy->__getLazyProperties() as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getTagline()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTagline', []);

        return parent::getTagline();
    }

    /**
     * {@inheritDoc}
     */
    public function setTagline($tagline)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTagline', [$tagline]);

        return parent::setTagline($tagline);
    }

    /**
     * {@inheritDoc}
     */
    public function getImage()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getImage', []);

        return parent::getImage();
    }

    /**
     * {@inheritDoc}
     */
    public function setImage(\User\Document\Social\Image $image)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setImage', [$image]);

        return parent::setImage($image);
    }

    /**
     * {@inheritDoc}
     */
    public function getAddress()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAddress', []);

        return parent::getAddress();
    }

    /**
     * {@inheritDoc}
     */
    public function setAddress(\User\Document\Social\Address $address)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAddress', [$address]);

        return parent::setAddress($address);
    }

    /**
     * {@inheritDoc}
     */
    public function getOccupation()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOccupation', []);

        return parent::getOccupation();
    }

    /**
     * {@inheritDoc}
     */
    public function setOccupation($occupation)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOccupation', [$occupation]);

        return parent::setOccupation($occupation);
    }

    /**
     * {@inheritDoc}
     */
    public function getResume()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getResume', []);

        return parent::getResume();
    }

    /**
     * {@inheritDoc}
     */
    public function setResume(\User\Document\Social\Resume $resume)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setResume', [$resume]);

        return parent::setResume($resume);
    }

    /**
     * {@inheritDoc}
     */
    public function getSkills()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSkills', []);

        return parent::getSkills();
    }

    /**
     * {@inheritDoc}
     */
    public function setSkills(\User\Document\Social\Skill\Skill $skills)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSkills', [$skills]);

        return parent::setSkills($skills);
    }

    /**
     * {@inheritDoc}
     */
    public function getAlbums()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAlbums', []);

        return parent::getAlbums();
    }

    /**
     * {@inheritDoc}
     */
    public function setAlbums(\User\Document\Social\Album $albums)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setAlbums', [$albums]);

        return parent::setAlbums($albums);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetExists($offset)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'offsetExists', [$offset]);

        return parent::offsetExists($offset);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetGet($offset)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'offsetGet', [$offset]);

        return parent::offsetGet($offset);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetSet($offset, $value)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'offsetSet', [$offset, $value]);

        return parent::offsetSet($offset, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function offsetUnset($offset)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'offsetUnset', [$offset]);

        return parent::offsetUnset($offset);
    }

    /**
     * {@inheritDoc}
     */
    public function toArr()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'toArr', []);

        return parent::toArr();
    }

    /**
     * {@inheritDoc}
     */
    public function toStdClass()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'toStdClass', []);

        return parent::toStdClass();
    }

    /**
     * {@inheritDoc}
     */
    public function toJSON()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'toJSON', []);

        return parent::toJSON();
    }

    /**
     * {@inheritDoc}
     */
    public function setIgnoredFields(array $fields)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setIgnoredFields', [$fields]);

        return parent::setIgnoredFields($fields);
    }

}
